import React, { Component } from "react";
import { connect } from "react-redux";
import { NavLink } from "react-router-dom";

class TodoDetails extends Component {
  render() {
    const { todo, id } = this.props.select_todos;
    return (
      <React.Fragment>
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <div className="detail">
                <NavLink className="btn btn-default btn-primary" to="/">
                  Back
                </NavLink>
                <p>Todo ID: {id}</p>
                <p>Todo Name: {todo}</p>
                <button className="btn btn-default btn-primary">Edit</button>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

function mapStateToProps(state) {
  return {
    select_todos: state.select_todos
  };
}

export default connect(mapStateToProps)(TodoDetails);
