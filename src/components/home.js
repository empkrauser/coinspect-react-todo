import React, { Component } from "react";
import { connect } from "react-redux";
import selectTodos from "../actions/action_select_todos";
import fetchTodos from "../actions/action_fetch_todos";
import { NavLink } from "react-router-dom";
import { bindActionCreators } from "redux";

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      todo_input: "",
      todo_list: []
    };
  }

  componentDidMount() {
    if (this.props.todos) {
      this.setState({
        todo_list: this.props.todos
      });
    }
  }

  onChange(e) {
    e.preventDefault();
    this.setState({
      todo_input: e.target.value
    });
  }

  onAddTodo(e) {
    e.preventDefault();
    const { todo_input, todo_list } = this.state;
    if (todo_input) {
      const newTodo = {
        todo: todo_input,
        id: Date.now()
      };

      this.setState({
        todo_list: todo_list.concat(newTodo),
        todo_input: ""
      });
    }
  }

  onRenderTodo() {
    const { todo_list } = this.state;
    this.props.fetchTodos(todo_list);

    return todo_list.map(obj => {
      return (
        <tr key={obj.id}>
          <td>{obj.todo}</td>
          <td>
            <NavLink
              className="btn btn-default btn-primary"
              onClick={() => {
                this.props.onSelectTodo(obj);
              }}
              to={`/${obj.id}`}
            >
              View
            </NavLink>
            <button
              className="btn btn-default btn-danger"
              onClick={this.onDeleteTodo.bind(this, obj.id)}
            >
              Delete
            </button>
          </td>
        </tr>
      );
    });
  }

  onDeleteTodo(id, e) {
    e.preventDefault();
    const { todo_list } = this.state;
    const newList = todo_list.filter(obj => {
      return obj.id !== id;
    });

    this.setState({
      todo_list: newList
    });
  }

  render() {
    const { todo_input } = this.state;
    return (
      <React.Fragment>
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <h1 className="text-center">List Your Todos</h1>
            </div>
            <div className="col-lg-4">
              <div className="input-group">
                <input
                  type="text"
                  className="form-control"
                  placeholder="add a todo"
                  value={todo_input ? todo_input : ""}
                  onChange={this.onChange.bind(this)}
                />
                <div className="input-group-append">
                  <button
                    className="btn btn-outline-secondary"
                    type="button"
                    onClick={this.onAddTodo.bind(this)}
                  >
                    Add
                  </button>
                </div>
              </div>
            </div>
            <div className="col-lg-8">
              <table className="table table-striped">
                <thead>
                  <tr>
                    <th scope="col">Todos</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>{this.onRenderTodo()}</tbody>
              </table>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

function mapStateToProps(state) {
  console.log(state);
  return {
    todos: state.todos
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      onSelectTodo: selectTodos,
      fetchTodos
    },
    dispatch
  );
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
