import React, { Component } from "react";

class App extends Component {
  render() {
    return (
      <div className="App">
        <br />
        <div className="main-content">
          <div className="grid-container">{this.props.children}</div>
        </div>
        <footer />
      </div>
    );
  }
}

export default App;
