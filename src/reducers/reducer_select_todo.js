export default function(state = null, action) {
  switch (action.type) {
    case "SELECT_TODOS":
      return action.payload;
  }

  return state;
}
