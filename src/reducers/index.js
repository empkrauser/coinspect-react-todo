import { combineReducers } from "redux";
import selectReducer from "./reducer_select_todo";
import fetchReducer from "./reducer_fetch_todo";

const rootReducer = combineReducers({
  todos: fetchReducer,
  select_todos: selectReducer
});

export default rootReducer;
