export const ACTION_NAME = "ACTION_NAME";

export default function functionName(payload) {
  return {
    type: ACTION_NAME,
    payload: payload
  };
}
