export const FETCH_TODOS = "FETCH_TODOS";

export default function fetchTodos(payload) {
  return {
    type: FETCH_TODOS,
    payload: payload
  };
}
