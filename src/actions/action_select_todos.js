export const SELECT_TODOS = "SELECT_TODOS";

export default function selectTodos(payload) {
  return {
    type: SELECT_TODOS,
    payload: payload
  };
}
